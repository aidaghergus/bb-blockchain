const SwapSign = artifacts.require("./SwapSign.sol");

contract("SwapSign", (accounts) => {
  describe("First Group of tests", () => {
    let swapSignInstance;
    before(async () => {
      swapSignInstance = await SwapSign.deployed();
    });

    it("...should return two addresses.", async () => {
      await swapSignInstance.addAgreement("0xaa", {
        from: accounts[0],
      });
      await swapSignInstance.signAgreement("0xaa", {
        from: accounts[1],
      });

      const signatures = await swapSignInstance.getSignatures("0xaa");
      console.log(signatures);

      assert.equal(signatures.length, 2, "Agreement not valid");
    });
  });
});
