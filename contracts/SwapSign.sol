pragma solidity >=0.4.21 <0.7.0;


contract SwapSign {
    struct Agreement {
        uint256 timestamp;
        bytes agreement_id;
        address[] signatures;
    }
    mapping(address => bytes[]) public users; //maps addresses to agreement id
    mapping(bytes32 => Agreement) public agreements; //maps keccak256(agreement_id) hashes to documents

    function addAgreement(bytes memory agreement_id) public {
        users[msg.sender].push(agreement_id); //Add agreement to users's "signed" list
        address[] memory sender = new address[](1);
        sender[0] = msg.sender;
        agreements[keccak256(agreement_id)] = Agreement(
            block.timestamp,
            agreement_id,
            sender
        );
    }

    function signAgreement(bytes memory id) public {
        users[msg.sender].push(id);
        agreements[keccak256(id)].signatures.push(msg.sender);
    }

    function getSignatures(bytes memory id) public view returns (address[] memory){
        return agreements[keccak256(id)].signatures;
    }
}
